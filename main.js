//створити новий HTML тег можна за допомогою document.createElement(element)
//Position - визначає куди буде додано елемент, може мати значення: beforebegin перед елементом, afterbegin в середині елемента, beforeend після дочірніх елементів, afterend після елементів.
//видалити елемент зі сторінки можна за допомогою element.remove().

function fun(array, op =document.body){
let items = array.map((item) => `<li>${item}</li>`).join('');

    if (op !== document.body) {
        op = document.createElement(op);
        document.body.append(op);
    }

    op.insertAdjacentHTML('afterbegin', `<ul>${items}</ul>`);
}

fun(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], 'div');